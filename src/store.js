import Vue from "vue"
import Vuex from "vuex"
import axios from "axios"
import { router } from "./router"

Vue.use(Vuex)

const store = new Vuex.Store({
    state : {
        token : "",
        fbAPIKey : "AIzaSyBxDmSlS5VTh1NAPykdIgzn3gwsWv7s-WQ",
        randevuListesi:[],
    },
    mutations : {
        setToken(state, token){
            state.token = token
        },
        clearToken(state){
            state.token = ""
        }/*,
        setRandevuListesi(state,randevu){
          state.randevuListesi.push(randevu);
        }*/
    },
    actions : {
        initAuth({ commit, dispatch}){
           let token = localStorage.getItem("token")
            if(token){

                let expirationDate = localStorage.getItem("expirationDate")
                let time = new Date().getTime()

                if(time >= +expirationDate){
                    console.log("token süresi geçmiş...")
                    dispatch("logout")
                } else {
                    commit("setToken", token)
                    let timerSecond = +expirationDate - time
                    console.log(timerSecond)
                    dispatch("setTimeoutTimer", timerSecond)
                    //router.push("/")
                    router.push("/").catch(()=>{})
                }

            } else {
                //router.push("/auth")
                router.push("/auth").catch(()=>{})
                return false
            }
        },
        login({ commit, dispatch, state}, authData){
            //let authLink = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key="

            let authLink = "http://localhost:8085/auth"
            if(authData.isUser){
              authLink = "http://localhost:8085/auth"
                //authLink = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key="
            }



            return axios.post(
                authLink ,//+ "AIzaSyBxDmSlS5VTh1NAPykdIgzn3gwsWv7s-WQ",
                { identifier :authData.user.email, password : authData.user.password}
                //{ email :authData.email, password : authData.password, returnSecureToken : true}
            ).then(response => {
                // console.log(response.data)

                //commit("setToken", response.data.idToken)
                //localStorage.setItem("token", response.data.idToken)

                commit("setToken", response.data.token)
                localStorage.setItem("token", response.data.token)

                //localStorage.setItem("expirationDate", new Date().getTime() + +response.data.expiresIn * 1000)
                localStorage.setItem("expirationDate", new Date().getTime() + +5*60000)
                // localStorage.setItem("expirationDate", new Date().getTime() + 10000)

                //dispatch("setTimeoutTimer", +response.data.expiresIn * 1000)
                dispatch("setTimeoutTimer", + 5 * 60000)
                // dispatch("setTimeoutTimer", 10000)
            })
        },
        randevuEkle({ commit, dispatch, state}, randevu){
            //let authLink = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key="

            state.randevuListesi.push(randevu.randevu);
            console.log(state.randevuListesi)
        },
        logout({ commit}){
            commit("clearToken")
            localStorage.removeItem("token")
            localStorage.removeItem("expirationDate")
            router.replace("/auth")
        },
        setTimeoutTimer({dispatch}, expiresIn){
            setTimeout(() => {
                dispatch("logout")
            }, expiresIn)
        }
    },
    getters : {
        isAuthenticated(state){
            return state.token !== ""
        },
        randevulariAl(state){
          return state.randevuListesi;
        }
    }
})

export default store
