import Vue from "vue"
import VueRouter from "vue-router"


import Homepage from "./pages/Homepage"
import Auth from "./pages/auth/Auth"
import RandevuOlustur from "./pages/RandevuOlustur"
import Randevular from "./pages/Randevular"

import store from "./store"

Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        {
            path: "/",
            component: Randevular,
            beforeEnter(to, from, next) {
                if (store.getters.isAuthenticated) {
                    next()
                } else {
                    next("/auth")
                }
            }
        },
        {
            path: "/yeniRandevu",
            component: RandevuOlustur,
            beforeEnter(to, from, next) {
                if (store.getters.isAuthenticated) {
                    next()
                } else {
                    next("/auth")
                }
            }
        },
        {
            path: "/Randevular",
            component: Randevular,
            beforeEnter(to, from, next) {
                if (store.getters.isAuthenticated) {
                    next()
                } else {
                    next("/auth")
                }
            }
        },
        {path: "/auth", component: Auth}

    ],
    mode: "history"
})
